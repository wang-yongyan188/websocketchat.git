package com.yan.wschat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yan.wschat.pojo.User;
import com.yan.wschat.pojo.dto.Result;
import com.yan.wschat.pojo.vo.UserFrom;

public interface IUserService extends IService<User> {
    Result login(UserFrom userFrom);

}
