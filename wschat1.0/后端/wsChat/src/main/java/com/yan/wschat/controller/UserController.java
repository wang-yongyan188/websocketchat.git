package com.yan.wschat.controller;

import com.yan.wschat.pojo.dto.Result;
import com.yan.wschat.pojo.dto.UserDTO;
import com.yan.wschat.pojo.vo.UserFrom;
import com.yan.wschat.service.IUserService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Resource
    private IUserService service;

    @PostMapping("/login")
    public Result test(@RequestBody UserFrom userFrom){

       return service.login(userFrom);
    }


}
