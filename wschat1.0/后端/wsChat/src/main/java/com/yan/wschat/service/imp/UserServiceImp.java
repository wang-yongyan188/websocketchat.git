package com.yan.wschat.service.imp;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yan.wschat.common.SystemConstants;
import com.yan.wschat.mapper.UserMapper;
import com.yan.wschat.pojo.User;
import com.yan.wschat.pojo.dto.Result;
import com.yan.wschat.pojo.dto.UserDTO;
import com.yan.wschat.pojo.vo.UserFrom;
import com.yan.wschat.service.IUserService;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImp extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public Result login(UserFrom userFrom) {
        User user = query().eq("name", userFrom.getName()).one();
        if(ObjectUtil.isNull(user)){
            return Result.fail("用户名或密码错误");
        }

        if(!user.getPassword().equals(userFrom.getPassword())){
            return Result.fail("用户名或密码错误");
        }

        String uuid = UUID.randomUUID().toString(true);
        UserDTO userDTO = new UserDTO();
        BeanUtil.copyProperties(user,userDTO);
        SystemConstants.map.put(uuid,userDTO);
        return Result.ok(uuid);
    }
}
