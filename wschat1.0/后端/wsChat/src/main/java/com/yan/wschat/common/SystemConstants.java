package com.yan.wschat.common;

import com.yan.wschat.pojo.dto.UserDTO;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SystemConstants {
    public static final Map<String, UserDTO> map=new ConcurrentHashMap<>();
    public static final String FRIEND_MSG="1";
    public static final String FRIEND_LIST="2";
    public static final String SYSTEM_INFO_UP="3";
    public static final String SYSTEM_INFO_DOWN="4";
    public static final String SYSTEM_INFO_INFO="5";
}
