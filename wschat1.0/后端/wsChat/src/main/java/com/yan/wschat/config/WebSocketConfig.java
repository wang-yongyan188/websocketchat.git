package com.yan.wschat.config;

import cn.hutool.core.util.ObjectUtil;
import com.yan.wschat.common.SystemConstants;
import com.yan.wschat.pojo.dto.UserDTO;
import com.yan.wschat.service.imp.UserServiceImp;
import com.yan.wschat.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import java.util.List;


@Configuration
@Slf4j
public class WebSocketConfig extends ServerEndpointConfig.Configurator {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {

        //获取请求头 请求头的名字是固定的
        List<String> list = request.getHeaders().get("Sec-WebSocket-Protocol");


        UserDTO userDTO = SystemConstants.map.get(list.get(0));

        UserHolder.saveUser(userDTO);
        if(ObjectUtil.isNull(userDTO)){

            throw new RuntimeException();
        }

        log.info("ws 请求发送方"+userDTO);

        //当Sec-WebSocket-Protocol请求头不为空时,需要返回给前端相同的响应
        response.getHeaders().put("Sec-WebSocket-Protocol",list);

        super.modifyHandshake(sec, request, response);
    }
}
