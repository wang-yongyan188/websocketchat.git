package com.yan.wschat.component;

import cn.hutool.json.JSONUtil;
import com.yan.wschat.common.SystemConstants;
import com.yan.wschat.config.WebSocketConfig;
import com.yan.wschat.pojo.dto.MsgBody;
import com.yan.wschat.pojo.dto.UserDTO;
import com.yan.wschat.service.imp.UserServiceImp;
import com.yan.wschat.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/wsserver/",configurator = WebSocketConfig.class)
@Component
@Slf4j
public class WebSocketServer {
    public static final Map<String,Session> sessionMap=new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session) throws IOException {
        MsgBody msgBody = new MsgBody().setType(SystemConstants.FRIEND_LIST);

        List<String> userDTOS = new ArrayList<>();
        MsgBody upMsg = new MsgBody().setType(SystemConstants.SYSTEM_INFO_UP).setMsg(UserHolder.getUser().getName());
        for (Map.Entry<String, Session> entry : sessionMap.entrySet()){
           if(!entry.getKey().equals(UserHolder.getUser().getName())){
               userDTOS.add(entry.getKey());
               entry.getValue().getBasicRemote().sendText(JSONUtil.toJsonStr(upMsg));
           }
        }
        msgBody.setMsg(userDTOS);
        session.getBasicRemote().sendText(JSONUtil.toJsonStr(msgBody));
        try{
            sessionMap.put(UserHolder.getUser().getName(),session);
        }
        catch (Exception e){
            log.info("没有登录");
        }
    }

    @OnMessage
    public void onMessage(String message) throws IOException{
        log.info("接收到数据"+message);
        MsgBody msgBody = JSONUtil.toBean(message, MsgBody.class);
        Session toUserSession = sessionMap.get(msgBody.getTo());

        String msg = JSONUtil.toJsonStr(new MsgBody().setType(SystemConstants.FRIEND_MSG)
                                        .setMsg(msgBody.getMsg()).setFrom(msgBody.getFrom()));

        toUserSession.getBasicRemote().sendText(msg);
    }
    @OnError
    public void onError(Session session, Throwable error) throws IOException {

        log.error("发生错误");
        toInformClose(session);

    }
    @OnClose
    public void onClose(Session session) throws IOException {
        log.info("关闭连接");
        toInformClose(session);
    }
    private void toInformClose(Session session) throws IOException {
        String name="";
        for (Map.Entry<String, Session> entry : sessionMap.entrySet()){

            if(entry.getValue()==session){
                name=entry.getKey();
            }
        }
        MsgBody msgBody = new MsgBody().setMsg(name).setType(SystemConstants.SYSTEM_INFO_DOWN);
        for (Map.Entry<String, Session> entry : sessionMap.entrySet()){

            if(entry.getKey()!=name){
                entry.getValue().getBasicRemote().sendText(JSONUtil.toJsonStr(msgBody));
            }
        }

    }
}
