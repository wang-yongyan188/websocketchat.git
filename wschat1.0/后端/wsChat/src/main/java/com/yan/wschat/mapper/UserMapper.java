package com.yan.wschat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yan.wschat.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
