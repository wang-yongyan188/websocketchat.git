import axios from 'axios'
import { Message } from 'element-ui';
// 配置项
const axiosOption = {
    baseURL: '/',
    timeout: 5000
}

// 创建一个单例
const instance = axios.create(axiosOption);

// 添加请求拦截器
instance.interceptors.request.use(function (config) {

  return config;
}, function (error) {
  return Promise.reject(error);
});

instance.interceptors.response.use(function (response) {
  let res= response.data
  if(!res.success){
    Message.error(res.errorMsg)
    return;
  }
  else{
    return res;
  }

}, function (error) {
  return Promise.reject(error);
});

export default instance;