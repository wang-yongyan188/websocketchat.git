import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/myLogin.vue')
  },
 
  {
    path: '/:name',
    name: 'myHome',
    component: () => import('../views/home/home.vue'),
    children: [
     
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
