import request from '@/utils/request.js'
const baseUrl='http://localhost:8888'
export function login(data){
    return request({
        url: baseUrl+'/user/login',
        method:'post',
        data
    });
}